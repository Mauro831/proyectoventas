<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
  	    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/stylus.css">
        <script src="js/javascript.js"></script>
        <title>Venta</title>
    </head>
    <body>
        
		<div class="container">
			<div class="row mt-3">
				<div class="col-12 tipo1">
					<h1 class="display-4">Venta de articulos</h1>
				</div>
			</div>
			<form action="" id="frmVenta" class="frmVenta mt-3">				
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						Tipo de Repoductor
					</div>
					<div class="col-md-6">
						<select name="" id="ddlmenu">
							<option class="opcionmenu"  value="nulo">Seleccione</option>
						    <option class="opcionmenu"  value="mp3">Mp3</option>
						    <option class="opcionmenu" value="mp4">Mp4</option>
						    <option class="opcionmenu" value="ipad">Ipad</option>
						</select>
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						Valor
					</div>
					<div class="col-md-6">
						<input type="text" id="txtvalor" readonly="">
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						Cantidad
					</div>
					<div class="col-md-6">
						<input type="number" min="1" id="txtCantidad">
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						Recargo
					</div>
					<div class="col-md-6">
						<input type="radio" name="porcentaje" value="5" checked="">5%
						<input type="radio" name="porcentaje" value="10" class="ml-2">10%
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						subtotal
					</div>
					<div class="col-md-6">
						<input type="text" id="txtSubTotal" readonly="">
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						Total Recargo
					</div>
					<div class="col-md-6">
						<input type="text" id="txtTotalRecargo" readonly="">
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-md-6 text-right">
						Total
					</div>
					<div class="col-md-6">
						<input type="text" id="txtTotal" readonly="">
					</div>
				</div>
				<div class="row mt-2">
					<div class="col-12 text-center">
						<input type="button" id="btnCalcular" class="btn btn-dark " value="Calcular">
					</div>
				</div>
			</form>
		</div>

        <script src="js/jquery-3.3.1.slim.min.js"></script>
		<script src="js/popper.min.js" "></script>
		<script src="js/bootstrap.min.js"></script>
    </body>
</html>